#include "Combination.h"
#include <iostream>

Combination::Combination(CombinationType combination, int cards[5]) :combination(combination)
{
	for (int i = 0; i < CARDS_IN_COMBINATION; i++)
		this->cards[i] = cards[i];
}

Combination::~Combination()
{
}

std::ostream &printCombinationName(std::ostream &out, Combination::CombinationType c)
{
	switch (c)
	{
	case Combination::HighCard: {out << "high card"; break; }
	case Combination::OnePair: { out << "pair"; break; }
	case Combination::TwoPairs: { out << "two pairs"; break; }
	case Combination::ThreeOfAKind: { out << "three of a kind"; break; }
	case Combination::Straight: { out << "straight"; break; }
	case Combination::Flush: { out << "flush"; break; }
	case Combination::FullHouse: { out << "full house"; break; }
	case Combination::FourOfAKind: { out << "four of a kind"; break; }
	case Combination::StraightFlush: { out << "straight flush"; break; }
	case Combination::RoyalFlush: { out << "royal flush"; break; }
	}
	return out;
}

bool Combination::operator==(Combination const& a)
{
	if (a.combination != this->combination || a.cards[0] != this->cards[0])
		return false;
	if (a.combination != Flush)
	{
		for (int i = 1; i < CARDS_IN_COMBINATION; i++)
		if (a.cards[i] != this->cards[i])
			return false;
	}
	return true;
}

Combination::CombinationType Combination::getCombinationType() const
{
	return combination;
}

int Combination::getCard(int index) const
{
	return cards[index];
}

std::ostream& operator<<(std::ostream& out, Combination& combination)
{
	for (int i = 0; i < Combination::CARDS_IN_COMBINATION; i++)
	{
		Card::printCard(out, combination.cards[i]);
		out << " ";
	}
	printCombinationName(out, combination.combination);
	return out;
}
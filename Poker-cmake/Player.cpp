#include "Player.h"
#include "Card.h"
#include "BadDecisionException.h"


Decision Player::makeDecision(int currentStake, std::vector<Decision::Action> possibleActions, int minStake, std::vector<int> board)
{
	Decision result = makeDecisionImpl(currentStake, possibleActions, minStake, board);
	if (isCorrect(result, possibleActions, currentStake, minStake))
	for (auto i = possibleActions.begin(); i != possibleActions.end(); i++)
	if (*i == result.getAction())
		return result;
	throw BadDecisionException(currentStake, result, *this, possibleActions);
}

bool Player::hasCard(int index)
{
	return cards[index];
}

int Player::getCurrentIndex()
{
	return currentIndex;
}

Player::Player()
{
}

Player::~Player()
{
}

Player::Player(int currentIndexArg, int money) : currentIndex(currentIndexArg), money(money)
{
}

std::string Player::getName()
{
	return name;
}

int Player::getMoney()
{
	return money;
}


int Player::getStake()
{
	return stake;
}

bool Player::isCorrect(Decision result, std::vector<Decision::Action> possibleDecisions, int currentStake, int minStake)
{
	bool exists = false;
	for (int i = 0; i < possibleDecisions.size(); i++)
	if (possibleDecisions[i] == result.getAction())
		exists = true;
	if (!exists)
		return false;
	if (result.getAction() == Decision::Action::Bet
		&& !(result.getStakeAmount() >= minStake || getMoney() < minStake))
		return false;
	if (result.getAction() == Decision::Action::Raise
		&& !(
			(result.getStakeAmount() >= minStake || getMoney() < minStake)
			&& (result.getStakeAmount() + getStake() > currentStake) 
			&& result.getStakeAmount() <= getMoney()
		))
		return false;
	if (result.getAction() == Decision::Action::Call
		&& !(result.getStakeAmount() <= getMoney() && result.getStakeAmount() < currentStake))
		return false;
	if (result.getAction() == Decision::Action::Check
		&& getStake() != currentStake)
		return false;
	return true;
}

int Player::getCardsCount() { return cardsCount; }

bool Player::isInGame() {
	return inGame;
}

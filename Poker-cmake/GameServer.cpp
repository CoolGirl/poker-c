// Poker (C++ homework).cpp : Defines the entry point for the console application.
//
#include <algorithm>
#include "CombinationLogics.h"
#include "GameServer.h"
#include "AIPlayer.h"
#include "BadDecisionException.h"
#include <time.h>


std::vector<int> GameServer::getCardsFromBoard()
{
	std::vector<int> board2(board);
	return board2;
}

GameServer::GameServer(int playersCount, std::string playerNames[], int money[], int stake) :minStake(stake)
{
	players = std::vector<Player*>(playersCount);
	for (int playerIndex = 0; playerIndex < playersCount; playerIndex++)
		players[playerIndex] = new ConsolePlayer(playerNames[playerIndex], playerIndex, money[playerIndex]);
	deckSize = Card::MAX_DECK_SIZE;
	playersInGame = playersCount;
	srand(time(NULL));
}

void GameServer::getBlinds()
{
	players[(buttonIndex + 1) % playersInGame]->money -= (players[(buttonIndex + 1) % playersInGame]->money > minStake / 2) ? (minStake / 2) :
		(players[(buttonIndex + 1) % playersInGame]->money);
	players[(buttonIndex + 1) % playersInGame]->stake = minStake / 2;
	players[(buttonIndex + 2) % playersInGame]->money -= (players[(buttonIndex + 2) % playersInGame]->money > minStake) ? (minStake) :
		(players[(buttonIndex + 2) % playersInGame]->money);
	players[(buttonIndex + 2) % playersInGame]->stake = minStake;
	currentStake = minStake;
}

std::vector<Decision::Action> GameServer::getPossibleDecision(int playerIndex, bool bet)
{
	std::vector<Decision::Action> possibleActions;
	if (currentStake == players[playerIndex]->stake)
		possibleActions.push_back(Decision::Action::Check);
	else
		possibleActions.push_back(Decision::Action::Call);
	if (bet)
		possibleActions.push_back(Decision::Action::Bet);
	else
	if (players[playerIndex]->getMoney() > (currentStake - players[playerIndex]->getStake()))
		possibleActions.push_back(Decision::Action::Raise);
	possibleActions.push_back(Decision::Action::Fold);
	return possibleActions;
}

int GameServer::takeCardFromDeck()
{
	int c = deck.back();
	deck.pop_back();
	return c;
}

void GameServer::giveCardsToPlayer(Player& player)
{
	player.cards[takeCardFromDeck()] = true;
}

std::vector<int> GameServer::giveMoneyToPlayers(std::vector<std::tuple<int, int>> playersPlaces) // key is place, value is playerNumber
{
	int pos = 0;
	int place = 1;
	int lastStake = 0;
	std::vector<int> result(players.size());
	while (pos < playersPlaces.size())
	{
		std::vector<int> samePlacesPlayers;
		while (pos<playersPlaces.size() && (std::get<0>(playersPlaces[pos]) == place))
		{
			samePlacesPlayers.push_back(std::get<1>(playersPlaces[pos]));
			pos++;
		}
		place++;
		std::sort(samePlacesPlayers.begin(), samePlacesPlayers.end(),
			[this](const int & firstPlayer, const int & secondPlayer) -> bool
		{
			return players[firstPlayer]->stake > players[secondPlayer]->stake;
		});
		for (int i = 0; i < samePlacesPlayers.size(); i++)
		{
			int playerIndex = samePlacesPlayers[i];
			if (players[playerIndex]->stake <= lastStake)
				continue;
			int money = 0;
			for (int j = 0; j < players.size(); j++)
			{
				int delta = std::min(players[j]->stake - lastStake, players[j]->stake);
				money += delta;
				players[j]->stake -= delta;
			}
			int potPart = money / (samePlacesPlayers.size() - i);
			for (int j = i; j < samePlacesPlayers.size(); j++)
			{
				result[samePlacesPlayers[j]] += potPart;
				players[samePlacesPlayers[j]]->money += potPart;
			}
			lastStake = players[playerIndex]->stake;
		}
	}
	return result;
}

void GameServer::putCardsOnBoard()
{
	board.push_back(takeCardFromDeck());
}

void GameServer::runRound(int roundNumber)
{
	int currentPlayersInGame = playersInGame;
	if (roundNumber == 2)
	for (int j = 0; j < 3; j++)
		putCardsOnBoard();
	else
	if (roundNumber != 1)
		putCardsOnBoard();
	bool bet = true;
	bool firstTrade = true;
	do
	{
		for (int i = 1; i <= playersInGame; i++)
		{
			if (currentPlayersInGame == 1)
				break;
			if (roundNumber == 1)
			{
				bet = false;
			}
			try
			{
				if (players[(buttonIndex + i) % playersInGame]->getMoney() != 0)
				{
					Decision playerDecision = players[(buttonIndex + i) % playersInGame]->makeDecision(currentStake,
						getPossibleDecision((buttonIndex + i) % playersInGame, bet), minStake, getCardsFromBoard());
					if (playerDecision.getAction() == Decision::Fold || players[(buttonIndex + i) % playersInGame]->getMoney() == 0)
					{
						players[(buttonIndex + i) % playersInGame]->inGame = false;
						currentPlayersInGame--;
					}
					if (playerDecision.getAction() == Decision::Action::Bet)
						bet = false;
					players[(buttonIndex + i) % playersInGame]->money -= playerDecision.getStakeAmount();
					players[(buttonIndex + i) % playersInGame]->stake += playerDecision.getStakeAmount();
					for (int k = 1; k <= playersInGame; k++)
					if (k != i)
						(players[(buttonIndex + k) % playersInGame])->notifyTurn(i, playerDecision);
					if (playerDecision.getAction() == Decision::Raise)
						currentStake = std::max(players[(buttonIndex + i) % playersInGame]->stake, currentStake);

				}

			}
			catch (BadDecisionException e)
			{
				std::cout << e.what();
			}
			if (!firstTrade && all_of(players.begin(), players.end(), [&](Player *p)
			{ return currentPlayersInGame == 1 || p->getMoney() == 0 || p->getStake() == currentStake; })) {
				break;
			}
		}
		firstTrade = false;
	} while (!all_of(players.begin(), players.end(), [&](Player *p)
	{
		return currentPlayersInGame == 1 || p->getMoney() == 0 || p->getStake() == currentStake;
	}));
	playersInGame = currentPlayersInGame;
}


void GameServer::deal()
{
	playersInGame = players.size();
	for (int i = 0; i < deckSize; i++)
		deck.push_back(i);
	for (int i = 0; i < deckSize; i++) {
		int j = rand() % deckSize;
		int t = deck[i];
		deck[i] = deck[j];
		deck[j] = t;
	}
	buttonIndex = (buttonIndex + 1) % playersInGame;
	getBlinds();
	for (int i = 0; i < players.size(); i++)
	for (int j = 0; j < 2; j++)
	{
		giveCardsToPlayer((*players[(buttonIndex + 1 + i) % playersInGame]));
	}
	for (roundNumber = 1; roundNumber <= ROUND_NUMBER; roundNumber++)
	{
		if (playersInGame != 1)
			runRound(roundNumber);
		else
		{
			roundNumber--;
			break;
		}
	}
}

void GameServer::startGame()
{
	buttonIndex = rand() % playersInGame;
	for (;;)
	{
		playersInGame = 0;
		for (int i = 0; i < players.size();i++)
		if (players[i]->getMoney() == 0)
			players[i]->inGame = false;
		else
			playersInGame++;
		if (playersInGame == 1)
		{
			std::cout << "One player survived. Game over.";
			break;
		}
		deal();
		std::vector<PlayerCombination> bestCombinations;
		for (int i = 0; i < board.size(); i++)
		{
			for (int j = 0; j < players.size(); j++)
				players[j]->cards[board[i]] = true;;
		}
		for (int i = 0; i < players.size(); i++)
		if (players[i]->inGame)
			bestCombinations.push_back(PlayerCombination(i, CombinationLogics::getBestCombination(players[i]->cards)));
		std::vector<std::tuple<int, int>> playersPlaces = CombinationLogics::getPlayersPlaces(bestCombinations);
		std::vector<int> money(giveMoneyToPlayers(playersPlaces));
		std::vector<Player*> players_copy(players.size());
		for (int i = 0; i < players.size(); ++i)
			players_copy[i] = players[i];
		if (roundNumber==1)
		for (auto i = bestCombinations.begin(); i != bestCombinations.end(); i++)
		{
			Combination &combination = std::get<1>(*i);
			combination.cards[2] = combination.cards[3] = combination.cards[4] = -1;
		}
			
			
		for (int i = 0; i < players.size(); ++i)
			players[i]->notifyGameResult(bestCombinations, playersPlaces, money, players_copy);
		for (int i = 0; i < players.size(); i++)
			players[i]->stake = 0;
		currentStake = 0;
		for (int i = 0; i < players.size(); i++)
		for (int j = 0; j < deckSize; j++)
			players[i]->cards[j] = false;
		board.resize(0);
	}
}

int GameServer::getMinStake()
{
	return minStake;
}

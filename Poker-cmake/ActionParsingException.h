#ifndef _H_ACTION_PARSING_EXCEPTION_
#define _H_ACTION_PARSING_EXCEPTION_
#ifndef _MSC_VER
#else
#define NOEXCEPT
#endif
#pragma once
#include <exception>

class ActionParsingException : public std::exception
{
public:
	ActionParsingException();
	~ActionParsingException();
	const char* what() const throw();
};
#endif //_H_ACTION_PARSING_EXCEPTION_


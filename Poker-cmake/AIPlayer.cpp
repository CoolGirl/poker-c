#include "AIPlayer.h"
#include "Player.h"

void AIPlayer::notifyTurn(int currentPlayerIndex, Decision decision)
{
}

Decision AIPlayer::makeDecisionImpl(int currentStake, std::vector<Decision::Action> possibleActions, int minStake, std::vector<int> board)
{
	return Decision();
}

void AIPlayer::notifyGameResult(std::vector<std::tuple<int, Combination>> bestCombinations, std::vector<std::tuple<int, int>> playersPlaces, std::vector<int> money, std::vector<Player*>)
{
}

AIPlayer::AIPlayer()
{
}

AIPlayer::AIPlayer(int currentIndex, int money) :Player(currentIndex,money)
{
}

AIPlayer::~AIPlayer()
{
}

#ifndef _H_CONSOLE_PLAYER_
#define _H_CONSOLE_PLAYER_
#pragma once
#include "Player.h"

class ConsolePlayer : public Player
{
	virtual void notifyTurn(int currentPlayerIndex, Decision decision) override;
	virtual void notifyGameResult(std::vector<std::tuple<int, Combination>> bestCombinations, std::vector<std::tuple<int, int>> playersPlaces, std::vector<int> money, 
		std::vector<Player*>) override;
    virtual Decision makeDecisionImpl(int currentStake, std::vector<Decision::Action> possibleActions, int minStake, std::vector<int> board) override;
	std::vector<std::string> notifications;
	static Decision::Action parseAction(std::string action);
public:
	ConsolePlayer();
	~ConsolePlayer();
	ConsolePlayer(std::string name, int currentIndex, int money) : Player(currentIndex, money) {
		Player::name = name;
	}
};
#endif //_H_CONSOLE_PLAYER_


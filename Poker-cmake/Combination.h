#ifndef _H_COMBINATION_
#define _H_COMBINATION_
#include "Card.h"
#pragma once

class Combination
{
public:
	enum CombinationType { HighCard, OnePair, TwoPairs, ThreeOfAKind, Straight, Flush, FullHouse, FourOfAKind, StraightFlush, RoyalFlush };
	static const int CARDS_IN_COMBINATION = 5;
	Combination(CombinationType combination, int cards[CARDS_IN_COMBINATION]);
	~Combination();
	bool operator ==(Combination const& a);

	//writes string representation of card combination in ostream
	friend std::ostream& operator<< (std::ostream &out, Combination& combination);

	// returns combination type of this combination according to Texas Hold'em rules
	CombinationType getCombinationType() const;

	// returns card index of index'th card in combination
	int getCard(int index) const;

private:
	CombinationType combination; 
	int cards[CARDS_IN_COMBINATION];
	friend class CombinationLogics;
	friend class GameServer;
};
#endif //_H_COMBINATION_


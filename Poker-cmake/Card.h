﻿#ifndef _H_CARD_
#define _H_CARD_
#pragma once
#include <string>


class Card
{
	Card();
public:
	static const int VALUES_NUMBER = 13;
	static const int SUITS_NUMBER = 4;
	static const int IMPOSSIBLE_CARD_INDEX = -1;
	enum CardSuit { Clubs = 'c', Diamonds = 'd', Hearts = 'h', Spades = 's' };
	enum CardValue { Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
	static const int MAX_DECK_SIZE = 52;

	// index from 0 to 51, it is formed as suit*VALUES_NUMBER+value
	static int getCardIndex(CardValue value, CardSuit suit);

	//	charRepresentation may be '2'..'9','T','J','Q','K','A'
	static CardValue parseCardValue(char charRepresentation);

	// returns first letter of suit for encoding compatibility
	static char getSuitChar(CardSuit suit);

	// return the char that can be parsed to the same value
	static char getValueChar(CardValue value);

	// cardIndex here and below is card position in 52-cards deck, for example, 11 == K♣
	static CardSuit getCardSuit(int cardIndex);

	// returns index from 0 to 3 
	static int getCardSuitIndex(int cardIndex);


	static CardValue getCardValue(int cardIndex);

	// writes to the ostream string representation of card as "suitValue"
	static std::ostream& printCard (std::ostream &out, int card);
};
#endif //_H_CARD_


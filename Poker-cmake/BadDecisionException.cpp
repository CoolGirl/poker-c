#include <sstream>
#include <iostream>
#include "BadDecisionException.h"


char const* BadDecisionException::what() const throw()
{
	std::stringstream msg;
	msg << "Player's money: " << player.getMoney() << ", his stake: " << result.getStakeAmount() << ", current stake: " << currentStake << ", decision: " <<
		Decision::actionToString(result.getAction()) << ".";
	for (int i = 0; i < possibleActions.size(); i++)
		msg << Decision::actionToString(result.getAction()) + " ";
	return msg.str().c_str();
}

BadDecisionException::~BadDecisionException()
{
}

BadDecisionException::BadDecisionException(int currentStake, Decision result, Player& player, std::vector<Decision::Action> possibleActions) :currentStake(currentStake),
result(result), player(player), possibleActions(possibleActions)
{
}
#pragma once
#ifndef _H_BAD_DECISION_EXCEPTION_
#define _H_BAD_DECISION_EXCEPTION_
#ifndef _MSC_VER
#else
#define NOEXCEPT
#endif
#include "Player.h"
class BadDecisionException:public std::exception
{
public:
	int currentStake;
	Decision result;
	Player& player;
	std::vector<Decision::Action> possibleActions;
	const char* what() const throw();
	~BadDecisionException();
	BadDecisionException(int currentStake, Decision result, Player& player, std::vector<Decision::Action> possibleActions);
};
#endif //_H_BAD_DECISION_EXCEPTION_


#include <algorithm>
#include "CombinationLogics.h"
#include <stdio.h>
#include <string.h>


CombinationLogics::CombinationLogics()
{
}


CombinationLogics::~CombinationLogics()
{
}

int CombinationLogics::findHighCard(int alreadyFoundCards[MAX_NUMBER_OF_HIGH_CARDS], bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER])
{
	for (int j = Card::VALUES_NUMBER - 1; j >= 0; j--)
	{
		if (valuesPlayerNumber[j] != 0)
		for (int k = 0; k < Card::SUITS_NUMBER; k++)
		if (cards[k*Card::VALUES_NUMBER + j] && k*Card::VALUES_NUMBER + j != alreadyFoundCards[0] 
			&& k*Card::VALUES_NUMBER + j != alreadyFoundCards[1] && k*Card::VALUES_NUMBER + j != alreadyFoundCards[2])
		{
			return (k*Card::VALUES_NUMBER + j);
		}
	}
	return -1;
}

Combination nullCombination()
{
	int cards[5];
	for (int i = 0; i < 5;i++)
	cards [i] = Card::IMPOSSIBLE_CARD_INDEX;
	return Combination(Combination::RoyalFlush, cards);
}
Combination CombinationLogics::findAnyFlush(bool cards[Card::MAX_DECK_SIZE], int suitsPlayerNumber[Card::SUITS_NUMBER], bool straight)
{
	int bestCombinations[Card::SUITS_NUMBER][Combination::CARDS_IN_COMBINATION];
	int numberInCombination = 4;
	bool wasFoundFlush = false;
	bool wasFoundStraight = false;
	for (int i = 0; i < Card::SUITS_NUMBER; i++)
	if (suitsPlayerNumber[i] >= 5)
	{
		wasFoundFlush = true;
		int oneSuitNeighbors = 0;
		for (int j = Card::VALUES_NUMBER - 1; j >= 0; j--)
		{
			if (cards[i*Card::VALUES_NUMBER + j])
				oneSuitNeighbors++;
			else if (straight)
				oneSuitNeighbors = 0;
			if (straight)
			{
				if (oneSuitNeighbors == 5)
				{
					for (int k = 0; k < Combination::CARDS_IN_COMBINATION; k++)
						bestCombinations[i][k] = i*Card::VALUES_NUMBER + j + k;
					wasFoundStraight = true;
					break;
				}
			}
			else
				bestCombinations[i][numberInCombination--] = i*Card::VALUES_NUMBER + j;
		}
		if (straight && oneSuitNeighbors == 4 && cards[i*Card::VALUES_NUMBER + Card::Ace])
		{
			bestCombinations[i][0] = i*Card::VALUES_NUMBER + Card::Ace;
			wasFoundStraight = true;
		}
	}
	if (wasFoundFlush && !straight || straight && wasFoundStraight)
	{
		int maxHighCard = 0;
		for (int i = 0; i < Card::SUITS_NUMBER; i++)
			maxHighCard = maxHighCard >= bestCombinations[i][Combination::CARDS_IN_COMBINATION - 1] ? maxHighCard : i;
		if (straight)
		{
			if (bestCombinations[maxHighCard][Combination::CARDS_IN_COMBINATION - 1] == Card::Ace)
				return Combination(Combination::RoyalFlush, bestCombinations[maxHighCard]);
				return Combination(Combination::StraightFlush, bestCombinations[maxHighCard]);
		}
		return Combination(Combination::Flush, bestCombinations[maxHighCard]);
	}
		return nullCombination();
}

Combination CombinationLogics::findStraightFlush(bool cards[Card::MAX_DECK_SIZE], int suitsPlayerNumber[Card::SUITS_NUMBER])
{
	return findAnyFlush(cards, suitsPlayerNumber, true);
}

bool CombinationLogics::wasFound(Combination combination)
{
	return combination.getCard(0)!=-1;
}

Combination CombinationLogics::findSomeOfAKind(bool cards[Card::MAX_DECK_SIZE],
	int someOfAKind, int valuesPlayerNumber[Card::VALUES_NUMBER], int alreadyFoundValue )
{
	int position = 0;
	for (int i = Card::VALUES_NUMBER - 1; i >= 0; i--)
	{
		if (valuesPlayerNumber[i] == someOfAKind && i!=alreadyFoundValue)
		{
			int answer[Combination::CARDS_IN_COMBINATION];
			for (int j = 0; j < Card::SUITS_NUMBER; j++)
			if (cards[Card::VALUES_NUMBER*j + i])
				answer[position++] = Card::VALUES_NUMBER*j + i;
			if (someOfAKind == 2)
				return Combination(Combination::OnePair, answer);
			if (someOfAKind == 3)
				return Combination(Combination::ThreeOfAKind, answer);
			if (someOfAKind == 4)
				return Combination(Combination::FourOfAKind, answer);
		}
	}
	return nullCombination();
}

Combination CombinationLogics::findFourOfAKind(bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER], int alreadyFoundValue)
{
	Combination answer = findSomeOfAKind(cards, 4, valuesPlayerNumber,alreadyFoundValue);
	if (wasFound(answer))
	{

		int alreadyFoundCards[3] = { answer.getCard(0), Card::IMPOSSIBLE_CARD_INDEX, Card::IMPOSSIBLE_CARD_INDEX };
		answer.cards[Combination::CARDS_IN_COMBINATION - 1] = findHighCard(alreadyFoundCards, cards, valuesPlayerNumber);
		return answer;
	}
	return answer;
}

Combination CombinationLogics::findThreeOfAKind(bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER], int alreadyFoundValue)
{
	Combination answer = findSomeOfAKind(cards, 3, valuesPlayerNumber, alreadyFoundValue);
	return answer;
}

Combination CombinationLogics::findFullHouseOrThreeOfAKind(bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER])
{
	Combination answer = findThreeOfAKind(cards, valuesPlayerNumber, Card::IMPOSSIBLE_CARD_INDEX);
	if (wasFound(answer))
	{
		Combination additionalAnswer = findTwoOfAKind(cards, valuesPlayerNumber, answer.cards[0]);
		if (wasFound(additionalAnswer))
		{
			int answerCards[Combination::CARDS_IN_COMBINATION];
			for (int i = 0; i < 3; i++)
				answerCards[i] = answer.cards[i];
			for (int i = 0; i < 2; i++)
				answerCards[i + 3] = additionalAnswer.cards[i];
			return Combination(Combination::FullHouse, answerCards);
		}
			int alreadyFoundCards[3] = { answer.cards[0], -1, -1 };
			answer.cards[Combination::CARDS_IN_COMBINATION - 2] = findHighCard(alreadyFoundCards, cards, valuesPlayerNumber);
			alreadyFoundCards[1] = answer.cards[Combination::CARDS_IN_COMBINATION - 2];
			answer.cards[Combination::CARDS_IN_COMBINATION - 1] = findHighCard(alreadyFoundCards, cards, valuesPlayerNumber);
			return answer;
	}
	return answer;
}

Combination CombinationLogics::findTwoOfAKind(bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER], int alreadyFoundValue)
{
	Combination answer = findSomeOfAKind(cards, 2, valuesPlayerNumber, alreadyFoundValue);
	return answer;
}

Combination CombinationLogics::findTwoPairs(bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER])
{
	Combination answer = findTwoOfAKind(cards, valuesPlayerNumber,-1);
	if (wasFound(answer))
	{
		Combination additionalAnswer = findTwoOfAKind(cards, valuesPlayerNumber, answer.cards[0]);
		int answerCard[Combination::CARDS_IN_COMBINATION];
		for (int i = 0; i < 2; i++)
			answerCard[i] = answer.cards[i];
		int alreadyFoundCards[3] = { answer.cards[0], additionalAnswer.cards[0], -1 };
		if (wasFound(additionalAnswer))
		{
			for (int i = 0; i < 2; i++)
				answerCard[i+2] = additionalAnswer.cards[i];
			answerCard[Combination::CARDS_IN_COMBINATION - 1] = findHighCard(alreadyFoundCards, cards, valuesPlayerNumber);
			return Combination(Combination::TwoPairs, answerCard);
		}
		else
		{
			for (int j = 1; j <= 3; j++)
			{
				answerCard[Combination::CARDS_IN_COMBINATION - 4 + j] = findHighCard(alreadyFoundCards, cards, valuesPlayerNumber);
				if (j!=3)
				alreadyFoundCards[j] = answerCard[Combination::CARDS_IN_COMBINATION - 4 + j];
			}
			return Combination(Combination::OnePair, answerCard);
		}
	}
	return answer;
}

Combination CombinationLogics::findFlush(bool cards[Card::MAX_DECK_SIZE], int suitsPlayerNumber[Card::SUITS_NUMBER])
{
	return findAnyFlush(cards, suitsPlayerNumber, false);
}

Combination CombinationLogics::findStraight(bool cards[Card::MAX_DECK_SIZE], int suitsPlayerNumber[Card::SUITS_NUMBER], int valuesPlayerNumber[Card::VALUES_NUMBER])
{
	int bestCombinations[Combination::CARDS_IN_COMBINATION];
	int neighbors = 0;
	for (int j = Card::VALUES_NUMBER - 1; j >= 0; j--)
	{
		if (valuesPlayerNumber[j])
		{
			neighbors++;
			if (neighbors == 5)
			{
				for (int k = 0; k < Combination::CARDS_IN_COMBINATION; k++)
				for (int i = 0; i < Card::SUITS_NUMBER; i++)
				if (cards[i*Card::VALUES_NUMBER + j])
				{
					bestCombinations[k] = cards[i*Card::VALUES_NUMBER + j + k];
					break;
				}
			}

		}
		else
			neighbors = 0;
	}
	if (neighbors == 5)
		return Combination(Combination::Straight, bestCombinations);
	if (neighbors == 4)
	{
		for (int j = 3; j >= 0;j--)
		for (int i = 0; i < Card::SUITS_NUMBER; i++)
		if (cards[i*Card::VALUES_NUMBER + j])
		{
			bestCombinations[j+1] = i*Card::VALUES_NUMBER + j;
		}
		for (int i = 0; i < Card::SUITS_NUMBER; i++)
		if (cards[i*Card::VALUES_NUMBER + Card::Ace])
			bestCombinations[0] = i*Card::VALUES_NUMBER + Card::Ace;
	}
	return nullCombination();
}

Combination CombinationLogics::getBestCombination(bool *cards)
{
	int valuesPlayerNumber[Card::VALUES_NUMBER];
	int suitsPlayerNumber[Card::SUITS_NUMBER];
	memset(valuesPlayerNumber, 0, sizeof(valuesPlayerNumber));
	memset(suitsPlayerNumber, 0, sizeof(suitsPlayerNumber));
	for (int i = 0; i < Card::MAX_DECK_SIZE; i++)
	{
		if (cards[i])
		{
			valuesPlayerNumber[Card::getCardValue(i)]++;
			suitsPlayerNumber[Card::getCardSuitIndex(i)]++;
		}
	}
	Combination threeOfAKind = nullCombination();
	//Combination threeOfAKind = nullCombination();
	Combination answer = findStraightFlush(cards, suitsPlayerNumber);
	if (wasFound(answer))
		return answer;
	answer = findFourOfAKind(cards, valuesPlayerNumber, Card::IMPOSSIBLE_CARD_INDEX);
	if (wasFound(answer))
		return answer;
	answer = findFullHouseOrThreeOfAKind(cards, valuesPlayerNumber);
	if (wasFound(answer))
	if (answer.getCombinationType() == Combination::ThreeOfAKind)
		threeOfAKind = answer;
	else
		return answer;
	answer = findFlush(cards, valuesPlayerNumber);
	if (wasFound(answer))
		return answer;
	answer = findStraight(cards, suitsPlayerNumber, valuesPlayerNumber);
	if (wasFound(answer))
		return answer;
	if (wasFound(threeOfAKind))
		return threeOfAKind;
	answer = findTwoPairs(cards, valuesPlayerNumber);
	if (wasFound(answer))
		return answer;
	answer = findTwoOfAKind(cards, valuesPlayerNumber, Card::IMPOSSIBLE_CARD_INDEX);
	if (wasFound(answer))
		return answer;
	int alreadyFoundCards[3] = { -1, -1, -1 };
	int cardsAnswer[Combination::CARDS_IN_COMBINATION];
	cardsAnswer[0] = findHighCard(alreadyFoundCards, cards, valuesPlayerNumber);
	int currentCard = 1;
	for (int i = 0; i < Card::MAX_DECK_SIZE && currentCard < 5; i++)
	if (cards[i] && i != cardsAnswer[0])
		cardsAnswer[currentCard++] = i;
	return Combination(Combination::HighCard, cardsAnswer);
}

std::vector<std::tuple<int, int>> CombinationLogics::getPlayersPlaces(std::vector<PlayerCombination> bestCombinations) //key is playerNumber
{
	std::sort(bestCombinations.begin(), bestCombinations.end(), [](const PlayerCombination & a, const PlayerCombination & b)
	{
		if (std::get<1>(a).getCombinationType() == std::get<1>(b).getCombinationType())
		{
			if (std::get<1>(a).cards[0] != std::get<1>(b).cards[0])
				return std::get<1>(a).cards[0] < std::get<1>(b).cards[0];
			if (std::get<1>(a).getCombinationType() == Combination::Flush)
				return true;
			for (int i = 0; i < Combination::CARDS_IN_COMBINATION; i++)
			if (std::get<1>(a).cards[i] != std::get<1>(b).cards[i])
				return std::get<1>(a).cards[i] < std::get<1>(b).cards[i];
			return false;
		}
		return std::get<1>(a).getCombinationType() < std::get<1>(b).getCombinationType();
	});
	std::vector <std::tuple<int, int>> places; // key is place, value is playerNumber
	int place = 0;
	for (int i = bestCombinations.size() - 1; i >= 0; i--)
	{
		if ( (i + 1)<bestCombinations.size() && std::get<1>(bestCombinations[i]) == std::get<1>(bestCombinations[i + 1]))
			places.push_back((std::tuple<int, int>(place, std::get<0>(bestCombinations[i]))));
		else
			places.push_back((std::tuple<int, int>(++place, std::get<0>(bestCombinations[i]))));
	}
	return places;
}
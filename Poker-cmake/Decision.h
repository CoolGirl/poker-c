#pragma once
#ifndef _H_DECISION_
#define _H_DECISION_
#include <string>

class Decision
{
public:
	enum Action
	{
		Check, Call, Raise, Fold, Bet, BadAction
	};
	Decision();

	// returns string representation of action ("check", "call", "raise", "bet", "fold")
	static std::string actionToString(Action action);

	// return desicion's stake
	int getStakeAmount() const;

	Action getAction() const;

	Decision(Action action, int stake);
private:
	int stakeAmount;
	Action action;
};

#endif //_H_DECISION_


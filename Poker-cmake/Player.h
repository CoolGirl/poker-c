#ifndef _H_PLAYER_
#define _H_PLAYER_
#pragma once
#include <string>
#include <vector>
#include <tuple>
#include "Card.h"
#include "Decision.h"
#include "Combination.h"

class Player
{
	int currentIndex;
	int money;
	int stake;
	int sidePot;
	bool cards[Card::MAX_DECK_SIZE];
	bool inGame = true;
	friend class GameServer;
	friend class BadDecisionException;
	int cardsCount;
protected:
    virtual Decision makeDecisionImpl(int currentStake, std::vector<Decision::Action> possibleActions, int minStake, std::vector<int> board) = 0;
	Decision makeDecision(int currentStake, std::vector<Decision::Action> possibleActions, int minStake, std::vector<int> board);
	virtual void notifyTurn(int currentPlayerIndex, Decision decision) = 0;
	virtual void notifyGameResult(std::vector<std::tuple<int, Combination>> bestCombinations, std::vector<std::tuple<int, int>> playersPlaces, std::vector<int> money, 
		std::vector<Player*>) = 0;
	int getCurrentIndex();
	bool isInGame();
	int getCardsCount();
	int getMoney();
	bool hasCard(int cardNumber);
	int getStake();
	bool isCorrect(Decision result, std::vector<Decision::Action> possibleActions, int currentStake, int minStake);
	std::string name;
public:
	Player();
	virtual ~Player();
	Player(int currentIndexArg, int money); 
	std::string getName(); 
};
#endif// _H_PLAYER_
#include <tuple>
#include <string>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include "ConsolePlayer.h"
#include "ActionParsingException.h"

void ConsolePlayer::notifyTurn(int currentPlayerIndex, Decision decision)
{
	std::stringstream ss;
	ss << "Player " << currentPlayerIndex << ": " << (Decision::actionToString(decision.getAction())) << decision.getStakeAmount() << " . ";
	notifications.push_back(ss.str());
}

void ConsolePlayer::notifyGameResult(std::vector<std::tuple<int, Combination>> bestCombinations, std::vector<std::tuple<int, int>> playersPlaces, std::vector<int> money, 
	std::vector<Player*> players)
{
	std::ostringstream ss;
	for (int i = 0; i < playersPlaces.size(); i++)
	{
		int playerIndex = std::get<1>(playersPlaces[i]);
		ss << players[playerIndex]->getName() << " has ";
		for (int j = 0; j < bestCombinations.size(); j++)
		if (std::get<0>(bestCombinations[j]) == playerIndex)
		{
			ss << std::get<1>(bestCombinations[j]) << " and wins ";
			ss << money[playerIndex] << "\n";
			break;
		}
	}
	notifications.push_back(ss.str());
}

Decision ConsolePlayer::makeDecisionImpl(int currentStake, std::vector<Decision::Action> possibleActions, int minStake, std::vector<int> board)
{
	for (int i = 1; i <= 50; i++)
		std::cout << "\n";
	std::cout << getName() << "'s turn. Press any key.";
	getchar(); getchar();
	for (auto i = notifications.begin(); i != notifications.end(); i++)
		std::cout << *i << "\n";
	notifications.clear();
	Decision playerDecision;
	int attemptNumber = 1;
	do
	{
		std::ostringstream msg;
		if (attemptNumber != 1) {
			msg << "You were mistaken. Please try again.";
			
		}
		msg << "Your cards:";
		for (int i = 0; i < Card::MAX_DECK_SIZE;i++)
		if (hasCard(i))
			Card::printCard(msg, i);
		for (auto i = board.begin(); i != board.end(); i++)
			Card::printCard(msg, *i);
		msg << ". Possible actions: ";
		for (int i = 0; i < possibleActions.size(); i++)
			msg << Decision::actionToString(possibleActions[i]) << " ";
		msg << ". Enter your decision:";
		std::cout << msg.str().c_str();
		std::string actionPlayer;
		std::cin >> actionPlayer;
		Decision::Action action =
			parseAction(actionPlayer);
		int stakePlayer = 0;
		if (action != Decision::Action::Fold && action != Decision::Action::Check && action!= Decision::Action::Call)
		{
			std::cout << ". Enter your stake:";
			std::string input;
			std::cin >> input;
			stakePlayer = atoi(input.c_str());
		}
		if (action == Decision::Action::Call)
			stakePlayer = currentStake - this->getStake();
		Decision aux(action, stakePlayer);
		playerDecision = aux;
		attemptNumber++;
	} while (!isCorrect(playerDecision, possibleActions, currentStake, minStake));
	return playerDecision;
}

Decision::Action ConsolePlayer::parseAction(std::string action)
{
	try
	{
		return action == "check" ? Decision::Check : action == "raise" ? Decision::Raise : action == "fold" ? Decision::Fold : action == "bet" ? Decision::Bet : action == "call" ?
			Decision::Call : throw ActionParsingException();
	}
	catch (ActionParsingException e)
	{
		return Decision::BadAction;
	}
}

ConsolePlayer::ConsolePlayer()
{
}


ConsolePlayer::~ConsolePlayer()
{
}
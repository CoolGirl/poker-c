﻿#include "Card.h"
#include <string>
#include <iostream>

Card::CardValue Card::parseCardValue(char charRepresentation)
{
	switch (charRepresentation)
	{
	case '2': return CardValue(0);
	case'3': return CardValue(1);
	case '4': return CardValue(2);
	case '5': return CardValue(3);
	case'6': return CardValue(4);
	case '7': return CardValue(5);
	case '8':return CardValue(6);
	case'9':return CardValue(7);
	case 'T': return CardValue(8);
	case 'J': return CardValue(9);
	case'Q':return CardValue(10);
	case 'K': return CardValue(11);
	case 'A': return CardValue(12);
	}
	return CardValue(13);
}

char Card::getSuitChar(CardSuit suit)
{
	switch (suit)
	{
	case Hearts: return 'h';
	case Diamonds: return 'd';
	case Spades: return 's';
	case Clubs: return 'c';
	}
	return '!';
}

char Card::getValueChar(CardValue value)
{
	switch (value)
	{
	case Two: return '2';
	case Three: return '3';
	case Four: return '4';
	case Five: return '5';
	case Six: return '6';
	case Seven: return '7';
	case Eight: return '8';
	case Nine: return '9';
	case Ten: return 'T';
	case Jack: return 'J';
	case Queen: return 'Q';
	case King: return 'K';
	case Ace: return 'A';
	}
	return '!';
}

Card::Card()
{
}

int Card::getCardIndex(CardValue value, CardSuit suit)
{
	return suit * (MAX_DECK_SIZE / 4) + value;
}

Card::CardSuit Card::getCardSuit(int cardIndex)
{
	int suitCode = cardIndex<VALUES_NUMBER? 'c': cardIndex<VALUES_NUMBER*2? 'd': cardIndex<VALUES_NUMBER*3? 'h': 's';
	return CardSuit(suitCode);
}

int Card::getCardSuitIndex(int cardIndex)
{
	int suitCode = cardIndex<VALUES_NUMBER ? 0 : cardIndex<VALUES_NUMBER * 2 ? 1 : cardIndex<VALUES_NUMBER * 3 ? 2 : 3;
	return suitCode;
}

Card::CardValue Card::getCardValue(int cardIndex)
{
	int valueCode = cardIndex % (MAX_DECK_SIZE / 4);
	return CardValue(valueCode);
}

std::ostream& Card::printCard(std::ostream& out, int card)
{
	if (card == -1)
		return out;
	return out << Card::getSuitChar(Card::getCardSuit(card)) << getValueChar(Card::getCardValue(card))<<" ";
}
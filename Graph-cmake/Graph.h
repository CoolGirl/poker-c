#pragma once

#include <functional>
#include <vector>
#include <unordered_map>
#include <fstream>
#include <stack>

template<typename NodePayload>
class Graph {
public:
	typedef int NodeHandle;
	typedef int EdgeHandle;
	static const NodeHandle MIN_NODE_HANDLE = 1;
	static const EdgeHandle MIN_EDGE_HANDLE = 0;
	typedef std::function<void(NodeHandle const &)> NodeVisitor;
	typedef std::function<void(NodeHandle const &)> EdgeVisitor;
	Graph();
	Graph(bool);
	~Graph();

	// loads graph from file in format in which saveToFile writes it to
	void loadFromFile(std::string const & filename);
	void saveToFile(std::string const & filename);


	NodeHandle addNode();

	// add edge from a to b if graph is oriented, add edge from a to b and b to a if it is not oriented
	void addEdge(NodeHandle const & a, NodeHandle const & b);

	// traverses all the graph' nodes in arbitrary order and calls visitor on each
	void forEachNode(std::function<void(NodeHandle const &)> const & visitor) const;


	size_t getNodesCount() const;

	// traverses all the graph's edges in arbitrary order and calls visitor on each
	void forEachEdge(NodeHandle const & source, EdgeVisitor const & visitor);

	// returns edge end for edge which starts from origin; throws runtime_error otherwise
	NodeHandle move(NodeHandle const & origin, EdgeHandle const & edge);

	// returns node's payload for node
	NodePayload & operator[](NodeHandle const & node);

	// traverses all the graph's nodes in dfs order; calls discoverNode when edge to a node is first seen, startNode when dfs enters the node, endNode
	// when dfs leaves it
	void dfs(NodeVisitor const & startNode, NodeVisitor const & endNode, NodeVisitor const & discoverNode);


private:
	struct Edge
	{
		NodeHandle from;
		NodeHandle to;
		Edge(NodeHandle from, NodeHandle to);
	};

	bool oriented;
	std::vector<std::vector<EdgeHandle>> edgesByNodes;
	std::vector<Edge> edges; //- ������ � ���� ������� ��� EdgeHandle
	std::vector<NodePayload> payloads;
};


template <typename NodePayload>
Graph<NodePayload>::Graph() : oriented(oriented)
{
}

template <typename NodePayload>
Graph<NodePayload>::Graph(bool oriented) : oriented(oriented)
{
}

template <typename NodePayload>
Graph<NodePayload>::~Graph()
{
}

template <typename NodePayload>
void Graph<NodePayload>::loadFromFile(std::string const& filename)
{
	std::ifstream input;
	input.open(filename);
	int edgeNumber;
	int vertexNumber;
	input >> oriented;
	input >> edgeNumber;
	input >> vertexNumber;
	edgesByNodes = std::vector<std::vector<EdgeHandle>>(vertexNumber);
	payloads = std::vector<NodePayload>(vertexNumber);
	for (int i = MIN_EDGE_HANDLE; i < edgeNumber; i++)
	{
		NodeHandle from;
		NodeHandle to;
		input >> from >> to;
		addEdge(from, to);
	}
	for (int i = MIN_NODE_HANDLE; i <= vertexNumber; i++)
		input >> (*this)[i];
	input.close();
}

template <typename NodePayload>
void Graph<NodePayload>::saveToFile(std::string const& filename)
{
	std::ofstream output;
	output.open(filename);
	int edgeNumber;
	int vertexNumber;
	output << oriented << "\n";
	int edgesCount = edges.size();
	if (!oriented)
	{
		edgesCount = 0;
		for (auto i = edges.begin(); i != edges.end(); ++i)
		if ((*i).from <= (*i).to)
			edgesCount++;
	}
	output << edgesCount << " " << getNodesCount() << "\n";
	for (int i = MIN_EDGE_HANDLE; i < edges.size(); i++)
	if (oriented || edges[i].from <= edges[i].to)
		output << edges[i].from << " " << edges[i].to << "\n";
	for (int i = MIN_NODE_HANDLE; i <= payloads.size(); i++)
		output << (*this)[i] << "\n";
	output.close();
}

template <typename NodePayload>
Graph<NodePayload>::Edge::Edge(NodeHandle from, NodeHandle to) :from(from), to(to)
{
}

template <typename NodePayload>
typename Graph<NodePayload>::NodeHandle Graph<NodePayload>::addNode()
{
	edgesByNodes.push_back(std::vector<EdgeHandle>());
	payloads.push_back(NodePayload());
	return static_cast<NodeHandle>(getNodesCount());
}

template <typename NodePayload>
void Graph<NodePayload>::addEdge(NodeHandle const& a, NodeHandle const& b)
{
	Edge currentEdge(a, b);
	edges.push_back(currentEdge);
	edgesByNodes[a - MIN_NODE_HANDLE].push_back(edges.size() - 1);
	if (!oriented && a != b)
	{
		Edge currentEdgeReversed(b, a);
		edges.push_back(currentEdgeReversed);
		edgesByNodes[b - MIN_NODE_HANDLE].push_back(edges.size() - 1);
	}
}

template <typename NodePayload>
void Graph<NodePayload>::forEachNode(std::function<void(NodeHandle const&)> const& visitor) const
{
	for (NodeHandle node_handle = MIN_NODE_HANDLE; node_handle <= getNodesCount(); node_handle++)
		visitor(node_handle);
}

template <typename NodePayload>
size_t Graph<NodePayload>::getNodesCount() const
{
	return edgesByNodes.size();
}

template <typename NodePayload>
void Graph<NodePayload>::forEachEdge(NodeHandle const& source, EdgeVisitor const& visitor)
{
	for (auto i = edgesByNodes[source-MIN_NODE_HANDLE].begin(); i != edgesByNodes[source-MIN_NODE_HANDLE].end(); ++i)
		visitor(edges[*i].to);
}

template <typename NodePayload>
typename Graph<NodePayload>::NodeHandle Graph<NodePayload>::move(NodeHandle const& origin, EdgeHandle const& edge)
{
	if (edges[edge].from == origin)
		return edges[edge].to;
	throw std::runtime_error("There's no such edge with such origin.");
}

template <typename NodePayload>
NodePayload& Graph<NodePayload>::operator[](NodeHandle const& node)
{
	return payloads[node - MIN_NODE_HANDLE];
}

template <typename NodePayload>
void Graph<NodePayload>::dfs(NodeVisitor const& startNode, NodeVisitor const& endNode, NodeVisitor const& discoverNode)
{
	std::vector<bool> visited(getNodesCount() + 1);
	std::vector<bool> discovered(getNodesCount() + 1);
	std::stack<NodeHandle> stack;
	for (NodeHandle node_handle = MIN_NODE_HANDLE; node_handle <= getNodesCount(); node_handle++)
	{
		if (!visited[node_handle])
		{
			stack.push(node_handle);
			while (!stack.empty())
			{
				NodeHandle top = stack.top();
				if (visited[top])
				{
					endNode(top);
					stack.pop();
				}
				else
				{
					visited[top] = true;
					startNode(top);
					for (auto i = edgesByNodes[top - MIN_NODE_HANDLE].begin(); i != edgesByNodes[top - MIN_NODE_HANDLE].end(); ++i)
					{
						auto to = edges[*i].to;
						if (!visited[to])
						{
							if (!discovered[to])
							{
								discoverNode(to);
								discovered[to] = true;
							}
							stack.push(to);
						}
					}
				}
			}
		}
	}
}
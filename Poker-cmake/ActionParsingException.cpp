#include <sstream>
#include <iostream>
#include "ActionParsingException.h"
#include "BadDecisionException.h"


ActionParsingException::ActionParsingException()
{
}


ActionParsingException::~ActionParsingException()
{
}

char const* ActionParsingException::what() const throw()
{
	std::stringstream msg;
	msg << "There is no such action in poker.";
	return msg.str().c_str();
}
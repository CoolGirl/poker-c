#include "GameServer.h"

int main(int ARGC, char* ARGV[])
{
	std::string playerNames[2] = { "Jane", "Sergey" };
	int money[2] = { 1000, 1000 };
	GameServer gameServer(2, playerNames, money, 100);
	gameServer.startGame();
}
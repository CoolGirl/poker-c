#include "Decision.h"
#include <string>


Decision::Decision()
{
}

std::string Decision::actionToString(Decision::Action action)
{
	switch (action)
	{
	case Decision::Check: return "check";
	case Decision::Call: return "call";
	case Decision::Raise: return "raise";
	case Decision::Fold: return "fold";
	case Decision::Bet: return "bet";
	}
}

int Decision::getStakeAmount() const
{
	return stakeAmount;
}

Decision::Action Decision::getAction() const
{
	return action;
}

Decision::Decision(Action action, int stake) : action(action), stakeAmount(stake)
{
}
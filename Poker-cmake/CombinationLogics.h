#ifndef  _H_COMBINATION_LOGICS_
#define _H_COMBINATION_LOGICS_
#pragma once
#include "Combination.h"
#include <tuple>

typedef std::tuple<int, Combination> PlayerCombination;

#include <vector>
#include "Card.h"

class CombinationLogics
{
	static const int MAX_NUMBER_OF_HIGH_CARDS = 3;
	static int findHighCard(int alreadyFoundValue[MAX_NUMBER_OF_HIGH_CARDS], bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER]);
	static Combination findAnyFlush(bool cards[Card::MAX_DECK_SIZE], int suitsPlayerNumber[Card::SUITS_NUMBER], bool straight);
	static Combination findStraightFlush(bool cards[Card::MAX_DECK_SIZE], int suitsPlayerNumber[Card::SUITS_NUMBER]);
	static bool wasFound(Combination combination);
	static Combination findSomeOfAKind(bool cards[Card::MAX_DECK_SIZE], int someOfAKind, int valuesPlayerNumber[Card::VALUES_NUMBER], int alreadyFoundValue);
	static Combination findFourOfAKind(bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER], int alreadyFoundValue);
	static Combination findThreeOfAKind(bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER], int alreadyFoundValue);
	static Combination findFullHouseOrThreeOfAKind(bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER]);
	static Combination findTwoOfAKind(bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER], int alreadyFoundValue);
	static Combination findTwoPairs(bool cards[Card::MAX_DECK_SIZE], int valuesPlayerNumber[Card::VALUES_NUMBER]);
	static Combination findFlush(bool cards[Card::MAX_DECK_SIZE], int suitsPlayerNumber[Card::SUITS_NUMBER]);
	static Combination findStraight(bool cards[Card::MAX_DECK_SIZE], int suitsPlayerNumber[Card::SUITS_NUMBER], int valuesPlayerNumber[Card::VALUES_NUMBER]);

public:
	CombinationLogics();
	~CombinationLogics();

	// returns the best combination according to Texas Hold'em rules
	static 	Combination getBestCombination(bool *cards);

	// returns pairs where keys = places, values = players indices
	static 	std::vector<std::tuple<int, int>> getPlayersPlaces(std::vector<PlayerCombination>bestCombinations);
};
#endif// _H_COMBINATION_LOGICS_


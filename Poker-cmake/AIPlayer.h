#ifndef _H_AIPLAYER_
#define _H_AIPLAYER_
#pragma once
#include "Player.h"

class AIPlayer:public Player
{
protected:
	virtual void notifyTurn(int currentPlayerIndex, Decision decision) override;
	virtual Decision makeDecisionImpl(int currentStake, std::vector<Decision::Action> possible_actions, int minStake, std::vector<int> board) override;
	virtual void notifyGameResult(std::vector<std::tuple<int, Combination>> bestCombinations, std::vector<std::tuple<int, int>> playersPlaces, std::vector<int> money,
		std::vector<Player*>) override;
	AIPlayer();
	AIPlayer(int currentIndex, int money);
	~AIPlayer();
};
#endif //_H_AIPLAYER_

#ifndef _H_GAME_SERVER_
#define _H_GAME_SERVER_
#include <stdio.h>
#include <iostream>
#include <vector>
#include "Player.h"
#include "Card.h"
#include "ConsolePlayer.h"

class GameServer
{
public:
	static const int MAX_CARDS_ON_BOARD = 5;
	static const int ROUND_NUMBER = 4;

	GameServer(int playersCount, std::string playerNames[], int money[], int stake);

	//starts the game which lasts as long as at least two players have money
	void startGame();


	int getMinStake();
	
private:
	std::vector<Player*> players;
	std::vector<int> places;
	std::vector<Card::CardValue> valuesInGame;
	std::vector<std::vector<int>> cardsOnBoard;
	Card::CardValue declaredCard;
	int deckSize;
	int onBoardCardsCount;
	int currentPlayerIndex;
	int handNumber = 1;
	int playersInGame;
	int buttonIndex;
	int minStake;
	int currentStake;
	std::vector<int> board;
	std::vector<int> deck;
	std::vector<int> getCardsFromBoard();
	void deal();
	void getBlinds();
	int takeCardFromDeck();
	void giveCardsToPlayer(Player& player);
	std::vector<int> giveMoneyToPlayers(std::vector<std::tuple<int, int>> playersPlaces);
	void putCardsOnBoard();
	std::vector<Decision::Action> getPossibleDecision(int playerIndex, bool bet);
	void runRound(int roundNumber);
	int roundNumber;
};
#endif //_H_GAME_SERVER